ESPHome Configurations
----------------------
Collection of my own ESPHome configurations

# Structure
- Common parameters are in [`common.yaml`](common.yaml)
- Strings and translations are in [`lang.yaml`](lang.yaml)
- Passwords and other secrets in [`secrets.yaml`](secrets.template.yaml) (Obviously not with my entries)
- DHT11-based Temperature / Humidity Sensor in [`sefa_air.yaml`](sefa_air.yaml)
- Tuya Blinds Switch (Flashed via TuyaConvert) in [`sefa_blinds.yaml`](sefa_blinds.yaml)

# Devices

## `sefa_air`
Temperature / Humidity Sensor.

### Components
- Board: NodeMCUv2
- BME280 Temperature / Pressure / Humidity Sensor

### Wiring
![sefa_air Wiring Diagram](assets/fritzing/sefa_air_bb.png)

### Flash
Via USB or OTA.

### `sefa_blinds`
Tuya-based Blinds Switch
![sefa_blinds Photo](assets/photos/sefa_blinds.jpg)

### Components
- Homecube Blinds / Cover Switch

### Wiring
N/A

### Flash
Via TuyaConvert or OTA

# License
This project is licensed under the terms of the GNU General Public License 3.0. You can read the full license
text in [LICENSE](LICENSE).
