Fritzing components needed 
--------------------------

- Adafruit Library (https://github.com/adafruit/Fritzing-Library)
  - DHT11 (found in `parts/DHT11 Humitidy and Temperature Sensor.fzpz`)
- ESP8266 Fritzing Parts (https://github.com/squix78/esp8266-fritzing-parts)
  - NodeMCU v1.0 (found in `nodemcu-v1.0/NodeMCUV1.0.fzpz`)
